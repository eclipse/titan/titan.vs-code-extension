# Titan changelog

## Unreleased

### Extension



### Language server

## [ 11.0.0 ] - 2024-11-14
- Titan Language Server improvements and bugfixes

## [ 10.1.2 ] - 2024-07-08
- Titanium code smell improvements


## [ 10.1.1 ] - 2024-06-05
- Better handling of excluded files
- Bugfixes


## [ 10.1.0 ] - 2024-04-25

### Extension

- Better handling of excluded files
- Dimming of inactive code in ttncpp files
- Indicating exclusion state for the active editor on the status bar
- Added documentation (can be opened using Ctrl-Shift-F1)
- Many bugfixes
- Usage of WSL2 as Windows alternative: having the WSL extension for VS Code makes the work with the Titan toolset quite comfortable. The guide is available [here](https://code.visualstudio.com/docs/remote/wsl)


### Language server

- Implemented build cancellation
- Several new semantic checks for document comments
- General stability and robustness improvements
- Bugfixes (12, 14, 15, 22, 28, 30, 31, 32, 33, 35, 37, 39, 40, 41, 42, 43, 45)


## [ 10.0.0 ] - 2023-11-15

Initial release. Version numbering is started from **10.0.0** to keep synch with existing Titan releases.