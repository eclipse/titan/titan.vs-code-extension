/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
* @author Csilla Farkas
*/

import { Selector } from "../selectors/Selector";

export abstract class MeasureButton {
    private button: HTMLButtonElement;
    protected selector: Selector;

    constructor(selector: Selector) {
        this.button = this.createButton();
        this.selector = selector;
    }

    private createButton(): HTMLButtonElement {
        const button = document.createElement('button');
        button.classList.add('measure-button');
        button.textContent = 'Measure';
        return button;
    }

    
    protected handleOnClick = (event: Event) => {};

    public getButton(): HTMLButtonElement {
        return this.button;
    }
}
