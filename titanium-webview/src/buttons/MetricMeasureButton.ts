
/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
* @author Csilla Farkas
*/
import { MeasureButton } from "./MeasureButton";
import { BaseSelector } from "../selectors/BaseSelector";
import { vscode } from "../common/constants";

export class MetricMeasureButton extends MeasureButton {
    constructor(selector: BaseSelector) {
        super(selector);
        this.getButton().addEventListener('click', this.handleOnClick);
    }
    protected handleOnClick = (event: Event) => {
        const selectedValue = this.selector.getSelectedValue();
        if (selectedValue === '') { return; }
        const message = {
            scope: 'module',
            identifier: vscode.getState()?.identifier,
            targetName: selectedValue,
            targetType: (this.selector as BaseSelector).getId()
        };
        console.log('message: ', message);
        vscode.postMessage(message);
    };
}