/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
* @author Csilla Farkas
*/

import { FullModuleStatistics } from "../common/interfaces";
import { MetricsFullStatTable }from "./MetricsFullStatTable";
import { BaseSelector } from "../selectors/BaseSelector";
import { MetricMeasureButton } from "../buttons/MetricMeasureButton";

export class ModuleStatTable extends MetricsFullStatTable {
    constructor(data: FullModuleStatistics, projectName: string) {
      super(data, projectName);
      this.title = `Module: ${data.name}`;
    }
  
    private addSelector(options: string[], selectorId: string, placeholder: string ): Element {
      const container = document.createElement('div');
      container.id = `${selectorId}-container`;
      container.classList.add('selector-container');
      const selector = new BaseSelector(options, selectorId, placeholder);
      container.appendChild(selector.getSelect());
      container.appendChild(new MetricMeasureButton(selector).getButton());
  
      return container;
    }
  
    public showTable(container: Element): void {
      this.removePrevTable();
      this.refreshTableTitle(container);
  
      const placeholderText = 'in module';
      const functionSelector = this.addSelector((this.data as FullModuleStatistics).functions, 'function', 'Functions ' + placeholderText);
      const altstepSelector = this.addSelector((this.data as FullModuleStatistics).altsteps, 'altstep', 'Altsteps ' + placeholderText);
      const testcaseSelector = this.addSelector((this.data as FullModuleStatistics).testcases, 'testcase', 'Testcases ' + placeholderText);
  
      container.appendChild(this.table);
      this.table.before(functionSelector, altstepSelector, testcaseSelector);
    }
}
