/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
* @author Csilla Farkas
*/

import { Selector } from "./Selector";

export class BaseSelector implements Selector {
    readonly options: string[];
    private select: HTMLSelectElement;

    constructor(options: string[], selectorId: string, placeholder: string) {
        this.options = options;
        this.select = this.createSelect(selectorId, placeholder);
    }

    private createSelect(selectorId: string, placeholderText: string): HTMLSelectElement {
        const select = document.createElement('select');
        select.id = selectorId;
        select.classList.add('selector');
        const placeholder = new Option(placeholderText, '', true, true);
        placeholder.disabled = true;
        select.add(placeholder);
        this.fillSelect(select);
        return select;
    }

    private fillSelect(select: HTMLSelectElement): void {
        this.options.forEach(optionName => select.add(new Option(optionName, optionName)));
    }

    public getSelectedValue(): string {
        return this.select.value;
    }

    public getSelect(): HTMLSelectElement {
        return this.select;
    }

    public getId(): string {
        return this.select.id;
    }
}