/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
* @author Csilla Farkas
*/

export interface StatisticColumns {
    DEV?: number | null,
    MAX?: number | null,
    MEAN?: number | null,
    TOTAL?: number | null
}

export interface Statistics {
    columns: StatisticColumns,
    highestRisk?: number
};

export interface MetricDataVal {
    name: string,
    statistics: StatisticColumns
}

export interface MetricDataStat {
    name: string,
    statistics: Statistics
}

export interface FullStatistics {
    ttcn3Modules: Record<string, string>,
    asn1Modules: Record<string, string>,
    functionMetrics: MetricDataStat[],
    altstepMetrics: MetricDataStat[],
    testcaseMetrics: MetricDataStat[]
}

export interface FullProjectStatistics extends FullStatistics {
    moduleMetrics: MetricDataStat[],
    projectMetrics: MetricDataVal[]
}

export interface FullModuleStatistics extends FullStatistics {
    name: string
    functions: string[],
    altsteps: string[],
    testcases: string[]
}

export interface Metrics {
    name: string,
    type: string,
    metrics: Record<string, number>
}

