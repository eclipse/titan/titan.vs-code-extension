# Excluding files/directories from the build

When a file is excluded from the build, it is indicated at different places on the user interface.
This is important information because there will be no **error / warning** markers for these files and most of the advanced editing features (like [_go to definition_](editing.md#go-to-definition), [_peek references_](editing.md#peek-references) or [_rename_](editing.md#rename) etc.) will not work for excluded files.

In explorer views and on the editor tab labels, excluded files and directories has an **E** letter indicating the excluded state. Also, file and directory names are displayed using a specific color. This color can be different depending on the color theme (for example it is purple when the _Dark+_ theme is set).

![Excluded item](img/excluded-explorer-tab.png)

<br><br>

For the currently edited file, there is also an indicator on the status bar with a _warning_ color. 

<br>

![Exclusion indicated on status bar](img/excluded-statusbar.png)
