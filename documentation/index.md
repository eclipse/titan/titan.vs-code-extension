# Titan extension

- [File exclusion](exclusion.md)
- [Code editing features](editing.md)
- [Extension settings](settings.md)

