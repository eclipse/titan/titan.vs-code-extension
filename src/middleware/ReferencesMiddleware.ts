/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Miklos Magyari
 */

import { ProvideReferencesSignature, ReferenceParams, ReferencesRequest } from "vscode-languageclient";
import client from "../client/titanClient";

export const provideReferencesSignature: ProvideReferencesSignature = (document, position, options, token) => {
     const params: ReferenceParams = {
          context: options,
          textDocument: {
               uri: document.uri.toString()
          },
          position
     };

     return client.sendRequest(ReferencesRequest.type, params, token)
          .then(
               (result) => {
                    if (token.isCancellationRequested) {
                         return null;
                    }
                    return client.protocol2CodeConverter.asReferences(result);
               },
               (error) => { 
                    return client.handleFailedRequest(ReferencesRequest.type, token, error.message, null, false);
               }
          );
};