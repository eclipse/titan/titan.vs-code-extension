/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Csilla Farkas
 */

import { CancellationToken, Uri, Webview, WebviewView, WebviewViewProvider, WebviewViewResolveContext, commands, workspace } from "vscode";
import { ClientManager, clientManager } from "../client/ClientManager";
import { Modal } from "../common/Modal";
import { settings } from "../common/Settings";
import { randomInt } from "crypto";

export class MetricsProvider implements WebviewViewProvider {
    public static readonly viewType = "metricsview";
    
    private _view?: WebviewView;
    
    constructor(
        private readonly _extensionUri: Uri
    ) {
        commands.executeCommand('setContext', 'titaniumEnabled', settings.titaniumEnabled);
    }

    public resolveWebviewView(
        webviewView: WebviewView,
        context: WebviewViewResolveContext,
        _token: CancellationToken) {
        
        webviewView.webview.options = {
            enableScripts: true,
            localResourceRoots: [
                Uri.joinPath(this._extensionUri, "titanium-webview")  
            ]
        };
        
        this._view = webviewView;
        webviewView.webview.html = this._getHtmlForWebview(webviewView.webview);

        webviewView.webview.onDidReceiveMessage(async (data) => {
            if(!ClientManager.isReady()) {
                await Modal.featureIsNotAvailable("Titanium metrics");
                return;
            }
            await this.handleMessageFromWebview(data);
        });

    }

    private async handleMessageFromWebview(data: any) {
        const { scope, targetName, targetType, identifier } = data;
        const requestParams = {
            scope,
            targetName,
            targetType,
            identifier: {
                uri: identifier !== "" ? Uri.file(identifier).toString() : identifier
            }
        };

        const metrics = await clientManager.getMetricData(requestParams);
        this._view?.webview.postMessage({
            projectName: workspace.name,
            scope,
            identifier,
            payload: metrics
        });
    }

    private _getHtmlForWebview(webview: Webview): string {
        const scriptUri = webview.asWebviewUri(Uri.joinPath(this._extensionUri, 'titanium-webview/dist', 'metrics.js'));
        const styleMetricsUri = webview.asWebviewUri(Uri.joinPath(this._extensionUri, 'titanium-webview/styles', 'metrics.css'));
        const nonce = this.getNonce();

        const html = `<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="Content-Security-Policy" content="default-src 'none'; style-src ${webview.cspSource}; script-src 'nonce-${nonce}';">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link href="${styleMetricsUri}" rel="stylesheet">
            <title>Titanium metrics</title>
        </head>
        <body>
            <div id="titanium-container">
                <section id="modules-container">
                </section>
                <section id="metrics-container">
                </section>
            </div>
            <script nonce="${nonce}" src="${scriptUri}"></script>
        </body>
        </html>`;

        return html;
    }

    private getNonce(): string {
        let text = '';
        const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 32; i++) {
            text += possible.charAt(randomInt(0, possible.length));
        }
        return text;
    }
}
