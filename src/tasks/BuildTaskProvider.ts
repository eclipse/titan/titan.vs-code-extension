/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * Build task provider to execute a titan core build of the project.
 * Implemented using CustomExecution because settings and environment must be queried for every run. 
 * 
 * @author Miklos Magyari
 */

import path = require("path");
import fs = require("fs");
import cp = require("child_process");
import { CancellationToken, CustomExecution, Event, EventEmitter, ProviderResult, Pseudoterminal, Task, TaskDefinition, TaskProvider, TerminalDimensions, window, workspace } from "vscode";
import { integer } from "vscode-languageclient";
import { ExclusionState } from "../file-exclusion/ExclusionState";

enum ErrorCode {
     success = '',
     unsupportedPlatform = 'Platform is unsupported',
     binDirMissing = 'Cannot find folder containing the compiler',
     makefilegenMissing = 'Cannot find `makefilegen`',
     compilerMissing = 'Cannot find compiler'
}

const kind: BuildTaskDefinition = {
     type: 'titanBuildTask',
     description: 'Titan core build'
};

export class BuildTaskProvider implements TaskProvider {
     static buildTaskProviderType = 'titanBuildTask';
     private promise: Thenable<Task[]> | undefined;

     provideTasks(token: CancellationToken): ProviderResult<Task[]> {
          if (this.promise === undefined) {
               this.promise = getBuildTasks();
          }
          return this.promise;
     }

     resolveTask(task: Task, token: CancellationToken): ProviderResult<Task> {
          throw new Error("Method not implemented.");
     }
}

class BuildTerminal implements Pseudoterminal {
     private writeEmitter = new EventEmitter<string>();
	onDidWrite: Event<string> = this.writeEmitter.event;
	private closeEmitter = new EventEmitter<number>();
	onDidClose?: Event<number> = this.closeEmitter.event;

     private folderName: string;
     private errorShown: boolean = false;

     constructor(folderName: string) {
          this.folderName = folderName;
     }

     open(initialDimensions: TerminalDimensions | undefined): void {
          this.performBuild();
     }

     close(): void {
          
     }

     private async performBuild(): Promise<void> {
          return new Promise<void>((resolve) => {
               const wsFolders = workspace.workspaceFolders;
               if (!wsFolders || wsFolders.length === 0 ) {
                    return;
               }

               let makefileGenName: string;
               let compiler: string;
               switch (process.platform) {
                    case 'win32':
                    case 'cygwin':
                         makefileGenName = 'makefilegen.exe';
                         compiler = 'compiler.exe';
                         break;
                    case 'linux':
                         makefileGenName = 'makefilegen';
                         compiler = 'compiler';
                         break;
                    default:
                         return;
               }

               const [errorcode, compilerPath] = checkCompiler(compiler, makefileGenName);
               
               let makefileGenFlags: string[] = [ 
                    '-f'
               ];
               
               if (errorcode !== ErrorCode.success) {
                    if (!this.errorShown) {
                         this.errorShown = true;
                         window.showErrorMessage(errorcode);
                    }
                    return;
               }
               if (compilerPath === undefined) {
                    return;
               }

               const makefileGen: string = path.join(compilerPath, makefileGenName);
               const fileList = getFileList(this.folderName).flatMap(elem => path.relative(this.folderName, elem)).join(' ').replace('\\', '/');
               const makefileGenParams: string = makefileGenFlags.join(' ') + ' ' + fileList;
               let child = cp.spawn(`cd ${this.folderName} && ${makefileGen} ${makefileGenParams} && make all`, { shell: true });
               child.stderr.on('data', data => {
                    this.output(data.toString());
               });
               child.stdout.on('data', data => {
                    this.output(data.toString());
               });
               child.on('close', (exitCode: integer) => {
                    this.closeEmitter.fire(exitCode);
                    resolve();
               });
          });
     }

     output(text: string) {
          let changed = text.replace(/([^\r])\n/g, '$1\r\n');
          this.writeEmitter.fire(changed);
     }
}

interface BuildTaskDefinition extends TaskDefinition {
     type: string;
     description: string;
}

async function getBuildTasks(): Promise<Task[]> {
     const tasks: Task[] = [];
     const wsFolders = workspace.workspaceFolders;
     if (!wsFolders || wsFolders.length === 0 ) {
          return tasks;
     }

     for (const folder of wsFolders) {
          const folderName = folder.uri.fsPath;
          if (!folderName) {
               continue;
          }
     
          const customTask = new Task(kind, folder, folderName + 'task', 'titanBuildTask', new CustomExecution(
               async(): Promise<Pseudoterminal> => {
                    return new BuildTerminal(folderName);
               }
          ));
          tasks.push(customTask);
     }

     return tasks;
}

function checkCompiler(compiler: string, makefilegen: string) : [ErrorCode, string|undefined] {
     let compilerDir = workspace.getConfiguration('titan.compiler');
     if (compiler === undefined) {
          return [ErrorCode.unsupportedPlatform, undefined];
     }

     let dirString: string|undefined;
     if (compilerDir === undefined) {
          dirString = process.env.TTCN3_DIR;
     } else {
          dirString = compilerDir.get('titanInstallationPath');
     }
     
     if (dirString === undefined) {
          return [ErrorCode.binDirMissing, undefined];
     }
     const fullpath = path.join(dirString, compiler);
     if (!fs.existsSync(fullpath)) {
          return [ErrorCode.compilerMissing, undefined];
     }

     return [ErrorCode.success, dirString];
}

function getFileList(dir: string) : string[] {
     const regex : RegExp = /^.*(ttcn|asn)$/;

     return fs.readdirSync(dir).flatMap((entry) => {
          const fullPath = path.join(dir, entry);
          if (fs.statSync(fullPath).isDirectory()) {
               return getFileList(fullPath);
          }

          return fullPath;
     }).filter(f => {
          if (ExclusionState.excludedItems.items.find(e => { 
               return e.fsPath === f;
          }) !== undefined) { 
               return false;
          }

          return f.match(regex);
     });
}