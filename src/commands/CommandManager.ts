/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Miklos Magyari
 * @author Csilla Farkas
 */

import { Disposable, commands } from "vscode";

export interface Command {
	readonly id: string;

	execute(...args: any[]): void | any;
}

export class CommandManager {
	private readonly commands = new Map<string, Disposable>();

	public dispose() {
		for (const registration of this.commands.values()) {
			registration.dispose();
		}
		this.commands.clear();
	}

	public register<T extends Command>(command: T): T {
		if (!this.commands.has(command.id)) {
			this.commands.set(command.id, commands.registerCommand(command.id, command.execute, command));
		}
		return command;
	}
}
