/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * 
 * @author Csilla Farkas
 */

import { ConfigurationTarget, Uri, workspace, TreeItemCollapsibleState } from "vscode";
import { TpdFile } from "../file-exclusion/exclude-by-tpd/TpdFile";
import { ConfigKey, TpdContextValue } from "./types";
import * as fs from 'fs';
import { settings } from "./Settings";

export abstract class Config<T, V> {
    abstract target: ConfigurationTarget;
    abstract configKey: ConfigKey;
    protected config = workspace.getConfiguration;

    abstract get rawContent(): any;
    abstract get content(): T | T[] | void;
    async update(args: V): Promise<void> {
        await this.config().update(this.configKey, args, this.target);
    }
}

export class ExcludeFromBuildConfig extends Config<Uri, string[]> { // string[] => Uri.fsPath[]
    target = ConfigurationTarget.Workspace;
    configKey = ConfigKey.excludeFromBuild;

    get rawContent(): string[] {
        const root = settings.projectRootUri!.fsPath;
        const files = (this.config().get(this.configKey) as string[]).map(file => root + file);
        return files.filter(file => fs.existsSync(file));
    }

    get content(): Uri[] {
        return this.rawContent.map(file => Uri.file(file));
    }
}

export class TpdFilesConfig extends Config<TpdFile, string[]> { // string[] => Uri.fsPath[]
    target = ConfigurationTarget.Workspace;
    configKey = ConfigKey.tpdFiles;

    get rawContent(): string[] {
        const files = this.config().get(this.configKey) as string[];
        return files.filter(file => fs.existsSync(file));
    }

    get content(): TpdFile[] {
        const tpds = this.rawContent;
        const tpdFiles = tpds.map(tpd => {
            const uri = Uri.file(tpd);
            return new TpdFile(TpdFile.formatTpdName(uri), TreeItemCollapsibleState.None, uri);
        });

        return tpdFiles;
    }
}

export class ExcludeByTpdConfig extends Config<boolean, boolean> {
    target = ConfigurationTarget.Workspace;
    configKey = ConfigKey.excludeByTpd;

    get rawContent(): boolean {
        return this.config().get(this.configKey) as boolean;
    }

    get content(): boolean {
        return this.rawContent;
    }
}

export class ActivatedTpdConfig extends Config<TpdFile, string> {
    target = ConfigurationTarget.Workspace;
    configKey = ConfigKey.activatedTpd;

    get rawContent(): string | void {
        const tpd = this.config().get(this.configKey) as string;
        if(fs.existsSync(tpd)) {
            return tpd;
        }
    }

    get content(): TpdFile | void {
        const rawContent = this.rawContent;
        if(rawContent) {
            const uri = Uri.file(rawContent);
            return new TpdFile(TpdFile.formatTpdName(uri), TreeItemCollapsibleState.None, uri, TpdContextValue.activated);
        }
    }
}