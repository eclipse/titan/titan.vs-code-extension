/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/

/**
 * Messages for notify the user
 * 
 * @author Csilla Farkas
 */

import { MessageOptions, window, workspace, Uri } from "vscode";
import { ConfigKey } from "./types";
import * as path from 'path';

export class Modal {
    public static async removeActivatedTpd(tpd: string) {
        const header = 'Removing activated tpd';
        const description = `${tpd} is currently active.\nRemoving it will deactivate the tpd.\nDo you want to proceed?`;
        const options: MessageOptions = {detail: description, modal: true};
        const buttons: string[] = ['Remove'];

        return await window.showInformationMessage(header, options, ...buttons);
    }

    public static async showExcludeMethodChangeInfo() {
        const isExcludeByTpd = workspace.getConfiguration().get(ConfigKey.excludeByTpd) as boolean;

        const description = `Exclusion method has been changed.\nYou can exclude files and folders from build ${isExcludeByTpd ? 'by activating a tpd' : ' in file explorer'}.`;
        return await window.showInformationMessage(description);
    }

    public static async failedToParseTpd(tpd: Uri, reason: string) {
        const { base } = path.parse(tpd.fsPath);
        const header = 'Activating invalid TPD';
        const description = `Failed to parse tpd: ${base}\n${reason}`;
        const options: MessageOptions = { detail: description, modal: true};
        await window.showErrorMessage(header, options);
    }

    public static async invalidLogFilePath(filePath: string) {
        const header = `Invalid log file path: ${filePath}`;
        const description = `Please set a valid absolute path to "logDir" in settings.\n\n"logs/titan-languageserver-java.log" can be found in the Titan extension source folder.`;
        await window.showWarningMessage(header, {detail: description, modal: true});
    }

    public static async featureIsNotAvailable(featureName: string, reason: string = "The language server is not initiated.") {
        const message = `${featureName} is not available now. Reason: ${reason}`;
        await window.showWarningMessage(message);
    }

    public static async fullProjectAnalyzisIsForbidden(issue: string) {
        const header = 'Cannot analyze project';
        const message = `${issue} requires 'titan.compiler.analyzeOnlyWhenTpdIsActive' to be turned off in Titan configuration settings.`;
        const options: MessageOptions = { detail: message, modal: true };
        const buttons: string[] = ['Turn off and analyze project', 'Analyze project by TPD'];
        return await window.showWarningMessage(header, options, ...buttons);
    }

    public static async allowFullProjectAnalyzisOrActivateTpd() {
        const header = 'Cannot analyze project';
        const message = 'Project analysis requires a TPD to be activated.';
        const options: MessageOptions = {detail: message, modal: true};
        const buttons: string[] = ['Allow full project analyzis', 'Activate a TPD'];
        return await window.showWarningMessage(header, options, ...buttons);
    }
}