/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Csilla Farkas
 * @author Miklos Magyari
 */

import { Uri, workspace, FileType } from "vscode";
import * as path from 'path';
import * as fs from 'fs';
import { settings } from "./Settings";

export class FileSystemManager {
    static async isDirectory(uri: Uri): Promise<boolean> {
        const fileData = await workspace.fs.stat(uri).then(
            (stat) => stat,
            () => null
        );

        return fileData !== null && fileData.type === FileType.Directory;
    }

    static getParent(uri: Uri): Uri {
        return uri.with({ path: path.posix.dirname(uri.path) });
    }

    static arePathsEqual(path1: string, path2: string): boolean {
        const file1Stats = fs.statSync(path1);
        const file2Stats = fs.statSync(path2);

        if (file1Stats.ino && file2Stats.ino) {
            return file1Stats.ino === file2Stats.ino;
        } else {
            return path.resolve(path1) === path.resolve(path2);
        }
    }

    static getParentsOfUriInWorkspace(uri: Uri): string[] {
        try {
            let inputPath = uri.fsPath;
            const parents: string[] = [];
            while (!this.arePathsEqual(inputPath, settings.projectRootUri!.fsPath)) {
                const parentDirPath = path.dirname(inputPath);
                if(!this.arePathsEqual(parentDirPath, settings.projectRootUri!.fsPath)) {
                    parents.push(Uri.file(parentDirPath).fsPath);
                }
                inputPath = parentDirPath;
            }
            return parents;

        } catch(e: unknown) {
            console.log(e);
            return [];
        }
    }

    static getDirectoryContents(dir: string): string[] {
        let contents: string[] = [];
        const files = fs.readdirSync(dir, { withFileTypes: true });
        for (const file of files) {
            const filePath = `${dir}/${file.name}`;
            contents.push(filePath);
            if (file.isDirectory()) {
                contents = [...contents, ...FileSystemManager.getDirectoryContents(filePath)];
            }
        }
        return contents;
    }

    static getDirContentUris(dir: Uri): Uri[] {
        const contents = this.getDirectoryContents(dir.fsPath);
        return contents.map(content => Uri.file(content));
    }

    static readWorkspace(): Map<string, Uri> {
        if (settings.projectRootUri) {
            const workspaceContent = this.getDirectoryContents(settings.projectRootUri.fsPath);
            return new Map<string, Uri>(workspaceContent.map(content => [path.normalize(content.toLowerCase()), Uri.file(path.normalize(content))]));
        }

        return new Map<string, Uri>();
    }
}