/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * 
 * @author Csilla Farkas
 */

import { window } from "vscode";
import { ResponseError } from "vscode-languageclient";


export abstract class CustomError {
    error: Error;
    errorMessage!: string;
    constructor(error: Error) {
        this.error = error;
        this.createErrorMessage();
    }
    logError(): void {
        console.error(this.errorMessage);
    }
    abstract createErrorMessage(): void;
    abstract showError(): void | Promise<void>;
}

export class ServerError extends CustomError {
    constructor(error: ResponseError<any>) {
        super(error);
    }

    createErrorMessage(): void {
        this.errorMessage = `Server error: ${this.error.message}`;
    }

    async showError(): Promise<void> {
        await window.showErrorMessage(this.errorMessage);
    }

    logError(): void {
        console.error(`[${new Date().toLocaleString()}] ${(this.error as ResponseError<any>).code} ${this.errorMessage}`);
    }
}

export class ExtensionError extends CustomError {
    constructor(error: Error) {
        super(error);
    }
    createErrorMessage(): void {
        this.errorMessage = `Extension error: ${this.error.message}`;
    }
    async showError(): Promise<void> {
        await window.showErrorMessage(this.errorMessage);
    }
}

function handleError(e: unknown) {
    let error: CustomError | null = null;
    if(e instanceof ResponseError) {
        error = new ServerError(e);
    } else {
        error = new ExtensionError(e as Error);
    }

    error?.showError();
}

export function catchError(target: any, key: string, desc: TypedPropertyDescriptor<(... params: any[]) => Promise<any>>): void {
    const method = desc.value;
    desc.value = async function (...args: any) {
        try {
            await method?.apply(this, args);
        } catch (e) {
            handleError(e);
        }
    };
}