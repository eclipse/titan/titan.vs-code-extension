/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Miklos Magyari
 * @author Csilla Farkas
 * @author Adam Knapp
 */

import { workspace, TextDocumentChangeEvent, TextDocument, ConfigurationChangeEvent,
    FileWillRenameEvent, WorkspaceFoldersChangeEvent, window, commands, Uri} from "vscode";
import { DidChangeTextDocumentParams, DidChangeTextDocumentNotification, DidOpenTextDocumentParams,
    DidOpenTextDocumentNotification, Event, DidChangeConfigurationNotification, DidChangeConfigurationParams } from 'vscode-languageclient';
import client from "../client/titanClient";
import DisposableManager from "../common/DisposableManager";
import { ConfigKey } from "../common/types";
import { Modal } from "../common/Modal";
import { mainExclusionManager } from "../file-exclusion/MainExclusionManager";
import { settings } from "../common/Settings";
import { clientManager } from "../client/ClientManager";
import { PROJECT_CONFIG_PATH } from "../client/FolderConfig";

export class WorkspaceEventManager extends DisposableManager {
    public register(event: Event<any>, handler: (event: any) => any ): void {
        const disposable = event(handler);
        this._disposables.push(disposable);
    }
}

const workspaceEventHandlers = {
    /** 
	 * A change is made to a file.
	 * If it has a file type of ttcn3, asn1 or config and it is still unsaved, we should notify the server.
	 **/
    notifyServerOfChangedUnsavedFile: (event: TextDocumentChangeEvent) => {
        if (event.document.isUntitled && (event.document.languageId === 'ttcn' ||
            event.document.languageId === 'asn1' || event.document.languageId === 'cfg')) {
            let contentChanges = event.contentChanges.map((change) => ({
                    range: { start: change.range.start, end: change.range.end },
                    text: change.text
                }));
    
            let params: DidChangeTextDocumentParams = {
                textDocument: { uri: Uri.file(event.document.fileName).toString(), version: event.document.version },
                contentChanges: contentChanges
            };
            client.sendNotification(DidChangeTextDocumentNotification.type, params);
        }
    },

    /** 
	 * A new file is created in the editor.
	 * If it has a file type of ttcn3, asn1 or config, we should notify the server.
	 **/
    notifyServerOfNewFileCreated: (document: TextDocument) => {
        if (document.isUntitled && (document.languageId === 'ttcn' ||
            document.languageId === 'asn1' || document.languageId === 'cfg')) {
            let params: DidOpenTextDocumentParams = {
                textDocument: {
                    languageId: document.languageId,
                    text: document.getText(),
                    uri: Uri.file(document.fileName).toString(),
                    version: 0
                }
            };
            client.sendNotification(DidOpenTextDocumentNotification.type, params);
        }
        return;
    },

    excludeFilesBeforeRename: (event: FileWillRenameEvent) => {
        event.waitUntil(mainExclusionManager.exclusionInExplorer.handleFileRename(event.files));        
    },

    refreshViewByExclusionMethod: async (event: ConfigurationChangeEvent) => {
        if (event.affectsConfiguration(ConfigKey.excludeByTpd)) {
            await Modal.showExcludeMethodChangeInfo();
        }
    },

    updateTitaniumEnabled: (event: ConfigurationChangeEvent) => {
        if (event.affectsConfiguration('titan.titaniumEnabled')) {
            commands.executeCommand('setContext', 'titaniumEnabled', settings.titaniumEnabled);
        }
    },
    
    /** send a didChangeConfiguration notification if an editor setting is changed */
    editorSettingsChanged: (event: ConfigurationChangeEvent) => {
        if (event.affectsConfiguration(ConfigKey.editor)) {
            let editor = workspace.getConfiguration(ConfigKey.editor);
            let params: DidChangeConfigurationParams = {
                settings: {
                    editor
                }
            };
            client.sendNotification(DidChangeConfigurationNotification.type, params);
        }
    },

    /** close editors if their parent folder is removed from the workspace. */
    changedFolders: async (event: WorkspaceFoldersChangeEvent) => {
        event.removed.map(removedFolder => {
            workspace.textDocuments.map(async doc => {
                if (doc?.uri?.fsPath?.startsWith(removedFolder.uri.fsPath)) {
                    await window.showTextDocument(doc, { preview: true, preserveFocus: false });
                    await commands.executeCommand('workbench.action.closeActiveEditor');
                }
            });
        });
    },

    documentSaved: (event: TextDocument) => {
        if (event.uri.toString().endsWith(PROJECT_CONFIG_PATH)) {
            clientManager.sendFolderConfig(event.uri, event.getText());
            return;
        }
    }
};

export function registerWorkspaceEvents(workspaceEventManager: WorkspaceEventManager) {
    workspaceEventManager.register(workspace.onDidChangeTextDocument, workspaceEventHandlers.notifyServerOfChangedUnsavedFile);
    workspaceEventManager.register(workspace.onDidOpenTextDocument, workspaceEventHandlers.notifyServerOfNewFileCreated);
    workspaceEventManager.register(workspace.onWillRenameFiles, workspaceEventHandlers.excludeFilesBeforeRename);
    workspaceEventManager.register(workspace.onDidChangeConfiguration, workspaceEventHandlers.refreshViewByExclusionMethod);
    workspaceEventManager.register(workspace.onDidChangeConfiguration, workspaceEventHandlers.updateTitaniumEnabled);
    workspaceEventManager.register(workspace.onDidChangeConfiguration, workspaceEventHandlers.editorSettingsChanged);
    workspaceEventManager.register(workspace.onDidChangeWorkspaceFolders, workspaceEventHandlers.changedFolders);
    workspaceEventManager.register(workspace.onDidSaveTextDocument, workspaceEventHandlers.documentSaved);
}
