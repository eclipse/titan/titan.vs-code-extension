/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Miklos Magyari
 */

import { settings } from "../common/Settings";
import { clientManager } from "./ClientManager";
import { Uri, workspace } from "vscode";

/** TODO : make this configurable */
export const PROJECT_CONFIG_PATH = '.TITAN_config.json';

/**
 * Support for sending folder configuration on startup
*/
export async function sendFolderConfiguration() {
     const folderConfig = `${settings.projectRootUri}/${PROJECT_CONFIG_PATH}`;
     const configUri = Uri.parse(folderConfig);
     try {
          const textDocument = await workspace.openTextDocument(configUri);
          let text = textDocument.getText();
          clientManager.sendFolderConfig(configUri, text);
     } catch (error) {
          console.error((error as Error).message);
     }
}
