/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Miklos Magyari
 */

import { clientManager } from "./ClientManager";

/**
 * Send environment variables available for the vscode process to the language server.
*/
export function sendEnvironmentVariables() {
     clientManager.sendEnvironmentVariables();
}
