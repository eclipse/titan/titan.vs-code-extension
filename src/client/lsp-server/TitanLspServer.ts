/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Csilla Farkas
 */

import { ChildProcessInfo, MessageTransports, StreamInfo } from 'vscode-languageclient/node';
import {settings} from "../../common/Settings";
import { ChildProcess } from "child_process";
import { ConnectionKind } from "../../common/types";
import { StdioCommunication } from './StdioCommunication';
import { TcpCommunication } from './TcpCommunication';

export class TitanLspServer {

    static start(): Promise<ChildProcess | StreamInfo | MessageTransports | ChildProcessInfo> {
        const connKind: ConnectionKind = settings.connectionKind;
        switch (connKind) {
            case ConnectionKind.stdio:
                return new StdioCommunication().launchServer();
            case ConnectionKind.tcp:
                return new TcpCommunication().launchServer();
            default:
                return new StdioCommunication().launchServer();
        }
    }
}
