/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Csilla Farkas
 * @author Miklos Magyari
 */

import * as fs from "fs";
import * as path from 'path';
import { ChildProcessInfo, MessageTransports, StreamInfo } from 'vscode-languageclient/node';
import { findJavaExecutable } from '../../common/utils';
import {settings} from "../../common/Settings";
import { spawn, ChildProcess, SpawnOptions } from "child_process";
import { window } from "vscode";

export abstract class LspServerLauncher {
    protected static _javaExecutablePath = findJavaExecutable('java');
    protected static _serverExecutableFileName = 'org.eclipse.titan.lsp.jar';
    protected static _serverExecutableFilePath = path.resolve(settings.extensionPath, 'server', LspServerLauncher._serverExecutableFileName);

    protected minHeapSize: string = '-Xms4g';
    protected maxHeapSize: string = '-Xmx8g';
    protected jar: string = '-jar';

    protected minimumJavaVersion = 11;

    protected childProcessOptions: SpawnOptions = {
        cwd: settings.projectRootUri?.fsPath
    };

    protected setupLogging(childProcess: ChildProcess): void {
        const logDir = settings.logDir;
        const logFilePath = path.join(logDir, settings.logFileName);
		try {
            if (!fs.existsSync(logDir)) {
                fs.mkdirSync(logDir);
            }

            const logStream = fs.createWriteStream(logFilePath, { flags: 'w' });
            logStream.on('error', error => console.log('Logging: ', error));
            childProcess.stderr?.pipe(logStream);

            console.log(`Storing log in '${logFilePath}'`);
        } catch (error) {
            console.error((error as Error).message);
        }
    }

    protected createChildProcess(args: string[], options: {}): ChildProcess {
        if (!LspServerLauncher._javaExecutablePath) {
            window.showErrorMessage('Java executable was not found. Cannot start Titan language server.');
            throw new Error('Java executable path was not found.');
        }
        const version = spawn(LspServerLauncher._javaExecutablePath, [ '-version'], { shell: true } );
        version.stderr.on('data', data => {
            const javaVersion = data.toString().match(/version "([^".]*)[^"]*"/);
            const majorVersion = parseInt(javaVersion[1]);
            console.log(`Detected java version ${majorVersion}`);
            if (this.minimumJavaVersion > majorVersion) {
                window.showErrorMessage(`Java version ${this.minimumJavaVersion} or higher required.`);
            }
        });
        const childProcess = spawn(LspServerLauncher._javaExecutablePath, args, options)
            .on('error', error => console.error(error.message))
            .on('exit', code => {
                    if (code) {
                        console.error('The process of language server exited with code: ', code);
                    }
                });
        return childProcess;
    }

    abstract launchServer(): Promise<ChildProcess | StreamInfo | MessageTransports | ChildProcessInfo >;
}