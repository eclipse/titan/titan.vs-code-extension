/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Csilla Farkas
 */

import { LspServerLauncher } from "./LspServerLauncher";
import { ChildProcess } from 'child_process';

export class StdioCommunication extends LspServerLauncher {
    async launchServer(): Promise<ChildProcess> {
        const args = [
            this.minHeapSize,
            this.maxHeapSize,
            this.jar,
            LspServerLauncher._serverExecutableFilePath,
        ];
        const childProcess = this.createChildProcess(args, this.childProcessOptions);
        this.setupLogging(childProcess);
        process.on('exit', () => childProcess.kill());

        return childProcess;
    }
}