/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * 
 * @author Csilla Farkas
 * @author Miklos Magyari
 *
 */

import { commands, FileSystemWatcher, StatusBarAlignment, ThemeColor, Uri, window, workspace } from "vscode";
import { ContextKey, ExcludedState } from "../../common/types";
import { FileSystemManager } from "../../common/FileSystemManager";
import { ExcludedFileDecorationProvider } from "../ExcludedFileDecorationProvider";
import { clientManager } from "../../client/ClientManager";
import { ExclusionState } from "../ExclusionState";
import { exclusionUtils } from "../ExclusionUtils";
import { settings } from "../../common/Settings";
import { stripRoot } from "../ExcludedItems";

export class ExclusionInExplorerManager {
    private _savedItemsToBeExcluded: Map<string, Uri> = new Map();
    private _buildButton = window.createStatusBarItem(StatusBarAlignment.Left, 100);
    watcher: FileSystemWatcher = workspace.createFileSystemWatcher('**');

    constructor() {
        this.watcher.onDidCreate(this._handleFileCreate);
        this.watcher.onDidDelete(this._handleFileDelete);
        this.setExcludedItemsContext();
        this._buildButton.command = 'titan.build';
        this._buildButton.text =`$(play) Build needed`;
        this._buildButton.backgroundColor = new ThemeColor('statusBarItem.errorBackground');
        this._buildButton.color = new ThemeColor('statusBarItem.errorForeground');
        this.updateBuildButton();
    }

    public async init() {
        await this._checkFileExplorer();
    }

    public updateBuildButton() {
        if (ExclusionState.isExclusionByTpd || ExclusionState.isEmptyTempExcludedItems) {
            this._buildButton.hide();
            return;
        }
        this._buildButton.show();
    }

    public createExcludedStateList(uris: Uri[], isExcluded: boolean): ExcludedState[] {
        return uris.map(item => ({identifier: {uri: item.toString()}, isExcluded: isExcluded} as ExcludedState));
    }

    private async _checkFileExplorer() {
        const workspaceContent = [...FileSystemManager.readWorkspace().values()];
        if (!workspaceContent.length) {
            return;
        }
        const itemsToBeExcluded = workspaceContent.filter(uri => !ExclusionState.excludedItems.in(uri) && ExclusionState.excludedItems.in(FileSystemManager.getParent(uri)));
        if(itemsToBeExcluded.length) {
            await this.exclude(itemsToBeExcluded);
        }
    }

    private async _processSelectedItems(uris: Uri[]): Promise<Uri[]> {
        const allSelectedItems: Uri[] = [];

        for (const uri of uris) {
            allSelectedItems.push(uri);
            const isDir = await FileSystemManager.isDirectory(uri);
            if(isDir) {
                const dirContent = FileSystemManager.getDirContentUris(uri);
                allSelectedItems.push(...dirContent);
            }
        }
        
        return allSelectedItems;
    }

    private async _hasOnlyExcludedContent(uri: Uri): Promise<boolean> {
        const isDir = await FileSystemManager.isDirectory(uri);
        if(isDir) {
            const dirContent = FileSystemManager.getDirContentUris(uri);
            return dirContent.every(uri => ExclusionState.excludedItems.in(uri));
        }

        return false;
    }

    private async _getListOfParentsBecomeExcluded(uri: Uri, selectedFilePaths: string[], parentsToBeExcluded: Uri[]) {
        const parent = FileSystemManager.getParent(uri);
        const parentContent = FileSystemManager.getDirContentUris(parent);
        const notExcludedContent = parentContent.filter(file => {
            let notExcluded = false;
            if(!ExclusionState.excludedItems.in(file) && !selectedFilePaths.includes(file.fsPath) && !parentsToBeExcluded.map(uri => uri.fsPath).includes(file.fsPath)) {
                notExcluded = true;
            }
            return notExcluded;
        });

        if(!notExcludedContent.length) {
            parentsToBeExcluded.push(parent);
            await this._getListOfParentsBecomeExcluded(parent, selectedFilePaths, parentsToBeExcluded);
        }
    };

    private async _findParentsToBeExcluded(uris: Uri[]): Promise<Uri[]> {
        const selectedFilePaths = uris.map(uri => uri.fsPath);
        const parentsToBeExcluded: Uri[] = [];

        let prevParent = Uri.file('');
        const root = settings.projectRootUri!;
        for (const uri of uris) {
            if (uri.fsPath === root.fsPath) {
                continue;
            }
            const parent = FileSystemManager.getParent(uri);
            if(parent.fsPath !== prevParent.fsPath) {
                prevParent = parent;
                await this._getListOfParentsBecomeExcluded(uri, selectedFilePaths, parentsToBeExcluded);
            }
        };

        return parentsToBeExcluded;
    }

    private _getAllUniqueParentDir(uris: Uri[]): Uri[] {
        const uniqueParentDirs = new Set<string>;
        uris.forEach(uri => {
            const parentDirs = FileSystemManager.getParentsOfUriInWorkspace(uri);
            parentDirs.forEach(parentPath => uniqueParentDirs.add(parentPath));
        });

        return [...uniqueParentDirs].map(parentPath => Uri.file(parentPath));
    }

    private _handleFileCreate = async (uri: Uri) => {
        await this.exclude([...this._savedItemsToBeExcluded.values()]);
        if(!this._savedItemsToBeExcluded.has(uri.fsPath) && ExclusionState.excludedItems.in(FileSystemManager.getParent(uri))) {
            await this.exclude([uri]);
        }
    };

    private _handleFileDelete = async (uri: Uri) => {
        await this.include([uri]);
        const parent = FileSystemManager.getParent(uri);
        if(await this._hasOnlyExcludedContent(parent)) {
            await this.exclude([parent]);
        }
    };

    public async setExcludedItemsContext() {
        const uriStrings = ExclusionState.excludedItems.items.map(uri => uri.toString());
        await commands.executeCommand('setContext', ContextKey.excludedItems, uriStrings);
    }

    public async handleFileRename(files: readonly {readonly oldUri: Uri, readonly newUri: Uri}[]) {
        this._savedItemsToBeExcluded.clear();

        for (const { oldUri, newUri } of files) {
            const newParent = FileSystemManager.getParent(newUri);
            if(ExclusionState.excludedItems.in(oldUri) || ExclusionState.excludedItems.in(newParent)) {
                this._savedItemsToBeExcluded.set(stripRoot(newUri.fsPath), newUri);
            }

            const isDir = await FileSystemManager.isDirectory(oldUri);
            if(isDir) {
                const dirContent = FileSystemManager.getDirContentUris(oldUri);
                dirContent.forEach(uri => {
                    if(ExclusionState.excludedItems.in(uri) || this._savedItemsToBeExcluded.has(newUri.fsPath)) {
                        const updatedPath = uri.fsPath.replace(oldUri.fsPath, newUri.fsPath);
                        this._savedItemsToBeExcluded.set(updatedPath, Uri.file(updatedPath));
                    }
                });
            }
        };
    }

    private async handleExcludeStateChange(uris: Uri[], isExcluded: boolean): Promise<void> {
        await this.setExcludedItemsContext();
        await ExcludedFileDecorationProvider.decorate(uris);
        clientManager.notifyServerIfExcludeStateChanged(this.createExcludedStateList(uris, isExcluded));
    }

    private async handleTempExcludeStateChange(uris: Uri[], isExcluded: boolean) {
        ExclusionState.saveTempItems(uris, isExcluded);
        await commands.executeCommand('setContext', ContextKey.tempExcludedItems, ExclusionState.tempExcludedItems);
        await commands.executeCommand('setContext', ContextKey.tempIncludedItems, ExclusionState.tempIncludedItems);
        await ExcludedFileDecorationProvider.decorate(uris);
        this.updateBuildButton();
    }

    async tempExclude(uris: Uri[]) {
        const allSelectedItems = await this._processSelectedItems(uris);
        const parentsToBeExcluded = await this._findParentsToBeExcluded(uris);
        const itemsToBeExcluded = [...allSelectedItems, ...parentsToBeExcluded];
        await this.handleTempExcludeStateChange(itemsToBeExcluded, true);
        exclusionUtils.showExcludedLabelExplorerExclusion(uris, true);
    }

    async exclude(uris: Uri[]): Promise<void> {
        const allSelectedItems = await this._processSelectedItems(uris);
        const parentsToBeExcluded = await this._findParentsToBeExcluded(uris);
        const itemsToBeExcluded = [...allSelectedItems, ...parentsToBeExcluded];
        await ExclusionState.excludedItems.add(itemsToBeExcluded);
        if(!ExclusionState.isExclusionByTpd) {
            await this.handleExcludeStateChange(itemsToBeExcluded, true);
        }
    };

    async tempInclude(uris: Uri[]) {
        const allSelectedItems = await this._processSelectedItems(uris);
        const parentDirs = this._getAllUniqueParentDir(allSelectedItems);
        const itemsToBeIncluded = [...allSelectedItems, ...parentDirs];
        await this.handleTempExcludeStateChange(itemsToBeIncluded, false);
        exclusionUtils.showExcludedLabelExplorerExclusion(uris, false);
    }

    async include(uris: Uri[]): Promise<void> {
        const allSelectedItems = await this._processSelectedItems(uris);
        const parentDirs = this._getAllUniqueParentDir(allSelectedItems);
        const itemsToBeIncluded = [...allSelectedItems, ...parentDirs];
        await ExclusionState.excludedItems.remove(itemsToBeIncluded);
        if(!ExclusionState.isExclusionByTpd) {
            await this.handleExcludeStateChange(itemsToBeIncluded, false);
        }
    };

    async build(): Promise<void> {
        if (ExclusionState.isEmptyTempExcludedItems) {
            return;
        }
        const tempExcludedItems = ExclusionState.tempExcludedItems;
        const tempIncludedItems = ExclusionState.tempIncludedItems;
        ExclusionState.excludedItems.add(tempExcludedItems);
        ExclusionState.excludedItems.remove(tempIncludedItems);
        const excludedStates = [
            ...this.createExcludedStateList(tempExcludedItems, true),
            ...this.createExcludedStateList(tempIncludedItems, false)
        ];
        await this.setExcludedItemsContext();
        ExclusionState.clearTempItems();
        await commands.executeCommand('setContext', ContextKey.tempExcludedItems, []);
        await commands.executeCommand('setContext', ContextKey.tempIncludedItems, []);
        await ExcludedFileDecorationProvider.decorate([...tempExcludedItems, ...tempIncludedItems]);
        clientManager.notifyServerIfExcludeStateChanged(excludedStates);
        this.updateBuildButton();
    }
}