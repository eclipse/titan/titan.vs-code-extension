/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * 
 * @author Csilla Farkas
 */


import { commands, Uri, workspace, window } from "vscode";
import { ConfigKey, ContextKey } from "../common/types";
import { ExclusionByTpdManager } from "./exclude-by-tpd/ExclusionByTpdManager";
import { ExcludedFileDecorationProvider } from "./ExcludedFileDecorationProvider";
import { TpdFileDecorationProvider } from "./exclude-by-tpd/TpdFileDecorationProvider";
import { ExclusionInExplorerManager } from "./exclude-in-explorer/ExclusionInExplorerManager";
import { clientManager } from "../client/ClientManager";
import { ExclusionState } from "./ExclusionState";
import { Modal } from "../common/Modal";
import { exclusionUtils } from "./ExclusionUtils";

class MainExclusionManager {
    public exclusionByTpd = new ExclusionByTpdManager();
    public exclusionInExplorer = new ExclusionInExplorerManager();

    constructor() {
        this.setContext(ContextKey.excludeByTpd, ExclusionState.isExclusionByTpd);
    }

    public async init() {
        if(ExclusionState.isExclusionByTpd) {
            await this.exclusionByTpd.init();
        } else {
            const configProp = workspace.getConfiguration(ConfigKey.compiler);
            if (configProp.get('analyzeOnlyWhenTpdIsActive') as boolean) {
                const result = await Modal.fullProjectAnalyzisIsForbidden('Project analysis without activated TPD file');
                if (result === 'Analyze project by TPD') {
                    await this.useTpdForFileExclusion();
                } else {
                    await configProp.update('analyzeOnlyWhenTpdIsActive', false);
                    clientManager.notifyServerIfExcludeStateChanged(this.exclusionInExplorer.createExcludedStateList(ExclusionState.excludedItems.items, true));
                    await this.exclusionInExplorer.init();
                }
            } else {
                clientManager.notifyServerIfExcludeStateChanged(this.exclusionInExplorer.createExcludedStateList(ExclusionState.excludedItems.items, true));
                await this.exclusionInExplorer.init();
            }
        }
        exclusionUtils.showExcludedLabel(window.activeTextEditor);
    }

    public async setContext(key: ContextKey, value: any) {
        await commands.executeCommand('setContext', key, value);
    }

    //exp => tpd
    public async useTpdForFileExclusion() {
        const tempItems = [...ExclusionState.tempExcludedItems, ...ExclusionState.tempIncludedItems];
        ExclusionState.clearTempItems();
        const filesToBeDecorated = [...ExclusionState.excludedItems.items, ...tempItems];
        await ExclusionState.switchToExclusionByTpd();
        await this.setContext(ContextKey.excludeByTpd, true);
        await this.setContext(ContextKey.excludedItems, []);
        await ExcludedFileDecorationProvider.decorate(filesToBeDecorated);
        this.exclusionInExplorer.updateBuildButton();
        await this.exclusionByTpd.init();
    }

    //tpd => exp
    public async useExplorerForFileExclusion() {
        const configProp = workspace.getConfiguration(ConfigKey.compiler);
        if (configProp.get('analyzeOnlyWhenTpdIsActive') as boolean) {
			const result = await Modal.fullProjectAnalyzisIsForbidden('File exclusion in explorer');
            if (result === 'Turn off and analyze project') {
                await configProp.update('analyzeOnlyWhenTpdIsActive', false);
            } else {
                return;
            }
		}
        const excludedByTpd = ExclusionState.excludedItems.fsPaths;
        ExclusionState.excludedItems.remove();
        await ExclusionState.switchToExclusionInExplorer();
        await this.exclusionInExplorer.setExcludedItemsContext();
        await this.setContext(ContextKey.excludeByTpd, false);
        const filesToBeDecorated = [...new Set([...excludedByTpd, ...ExclusionState.excludedItems.items.map(item => item.fsPath)])].map(file => Uri.file(file));
        this.exclusionByTpd.tpdFiles.deactivateTpd();
        this.exclusionByTpd.refresh();
        clientManager.notifyServerIfExcludeStateChanged(this.exclusionInExplorer.createExcludedStateList(ExclusionState.excludedItems.items, true));
        TpdFileDecorationProvider.decorate(this.exclusionByTpd.tpdFiles.items.map(tpd => tpd.resourceUri));
        await ExcludedFileDecorationProvider.decorate(filesToBeDecorated);
    }
}

export const mainExclusionManager = new MainExclusionManager();