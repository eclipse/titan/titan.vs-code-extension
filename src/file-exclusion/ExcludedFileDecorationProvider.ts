/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Csilla Farkas
 */

import { window, FileDecorationProvider, Disposable, Uri, CancellationToken, FileDecoration, ThemeColor, EventEmitter } from 'vscode';
import { ExclusionState } from './ExclusionState';

export class ExcludedFileDecorationProvider implements FileDecorationProvider {
    private _disposables: Disposable[] = [];

    private _decorationForExcludedByFileExplorer = new FileDecoration('E', 'Excluded from build', new ThemeColor('charts.purple'));
    private _decorationForExcludedByTpd = new FileDecoration('E', 'Excluded from build by activated tpd', new ThemeColor('list.deemphasizedForeground'));
    private _decorationForTempExcludedFiles = new FileDecoration('SE', 'Selected for exclude from build', new ThemeColor('textLink.foreground'));
    private _decorationForTempIncludedFiles = new FileDecoration('SI', 'Selected for include for build', new ThemeColor('textLink.foreground'));

    constructor() {
      this._disposables.push(window.registerFileDecorationProvider(this));
    }

    static eventEmitter = new EventEmitter<Uri[]>;
    onDidChangeFileDecorations = ExcludedFileDecorationProvider.eventEmitter.event;

    provideFileDecoration(uri: Uri, token: CancellationToken) {
        const tempStatus = ExclusionState.getTempStatus(uri);
        if (tempStatus !== undefined) {
            return tempStatus ? this._decorationForTempExcludedFiles : this._decorationForTempIncludedFiles;
        }

        if(ExclusionState.isExcluded(uri)) {
            return ExclusionState.isExclusionByTpd ? this._decorationForExcludedByTpd : this._decorationForExcludedByFileExplorer;
        }
    }

    static async decorate(files: Uri[]) {
        if(files.length > 100) {
            const batchSize = 100;
            let index = 0;
            while (index < files.length) {
                const batch = files.slice(index, index + batchSize);
                index += batchSize;
                this.eventEmitter.fire(batch);
                await new Promise(resolve => setTimeout(resolve, 0));
            }
        } else {
            this.eventEmitter.fire(files);
        }
    }

    dispose() {
        this._disposables.forEach((disposable) => disposable.dispose());
    }
}
