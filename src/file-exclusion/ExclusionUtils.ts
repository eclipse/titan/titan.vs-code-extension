/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * 
 * @author Miklos Magyari
 */

import { StatusBarAlignment, StatusBarItem, TextEditor, ThemeColor, Uri, window } from "vscode";
import { ExclusionState } from "./ExclusionState";

class ExclusionUtils {
     statusLabel: StatusBarItem;
     initialized = false;

     constructor() {
          this.statusLabel = window.createStatusBarItem(StatusBarAlignment.Right, 100);
          this.statusLabel.color = new ThemeColor('statusBarItem.warningForeground');
          this.statusLabel.backgroundColor = new ThemeColor('statusBarItem.warningBackground');
          this.statusLabel.text = 'Excluded file';
          this.statusLabel.tooltip = 'This file is excluded from build. Advanced code editing features will not work.';
     }

     public showExcludedLabel(arg: TextEditor | null | undefined) {
          if (arg === null || arg === undefined) {
               return;
          }

          this.statusLabel.hide();
          if (ExclusionState.excludedItems.items.find(e => e.toString() === arg.document.uri.toString()) !== undefined || 
               ExclusionState.tempExcludedItems.find(e => e.toString() === arg.document.uri.toString()) !== undefined) {
               this.statusLabel.show();
          }
     }

     public showExcludedLabelExplorerExclusion(uris: Uri[], isExclude: boolean) {
          const activeEditorFile = window.activeTextEditor?.document.uri.toString();

          if (uris.find(e => e.toString() === activeEditorFile) !== undefined) {
               if (isExclude) {
                    this.statusLabel.show();
               } else {
                    this.statusLabel.hide();
               }
          }
     }
}

export const exclusionUtils = new ExclusionUtils();