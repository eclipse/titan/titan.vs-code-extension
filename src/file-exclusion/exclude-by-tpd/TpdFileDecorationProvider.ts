/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Csilla Farkas
 */

import { window, FileDecorationProvider, Disposable, Uri, CancellationToken, FileDecoration, ThemeColor, EventEmitter } from 'vscode';
import { ExclusionState } from '../ExclusionState';

export class TpdFileDecorationProvider implements FileDecorationProvider {
    private _disposables: Disposable[] = [];

    private _decorationForActivatedTpd = new FileDecoration('A', "Activated tpd", new ThemeColor('list.activatedTpd'));

    constructor() {
      this._disposables.push(window.registerFileDecorationProvider(this));
    }

    static eventEmitter = new EventEmitter<Uri[]>;
    onDidChangeFileDecorations = TpdFileDecorationProvider.eventEmitter.event;

    provideFileDecoration(uri: Uri, token: CancellationToken) {
        // ExclusionState.isExclusionByTpd &&
        if(ExclusionState.isActivatedTpd(uri)) {
            return this._decorationForActivatedTpd;
        }
    }

    static decorate(files: Uri[]) {
        TpdFileDecorationProvider.eventEmitter.fire(files);
    }

    dispose() {
        this._disposables.forEach((disposable) => disposable.dispose());
    }
}