/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * 
 * @author Csilla Farkas
 */


import { TreeItem, Uri, TreeItemCollapsibleState } from "vscode";
import { TpdContextValue } from "../../common/types";
import { clientManager } from "../../client/ClientManager";
import * as path from 'path';
import { FileSystemManager } from "../../common/FileSystemManager";

export class TpdFile extends TreeItem {
    constructor(
        public readonly label: string,
        public readonly collapsible: TreeItemCollapsibleState,
        public readonly resourceUri: Uri,
        public contextValue: TpdContextValue = TpdContextValue.deactivated
    ) {
        super(label, collapsible);
    }
    
    static formatTpdName(uri: Uri): string {
        const {base, dir} = path.parse(uri.fsPath);
        return `${base} - ${dir}`;
    }
    
    public get isActivated() {
        return this.contextValue === TpdContextValue.activated;
    }

    public activate() {
        this.contextValue = TpdContextValue.activated;
    }

    public deactivate() {
        this.contextValue = TpdContextValue.deactivated;
    }

    public async getFilesToBeExcluded(): Promise<Uri[] | void> {
        const fileList = await clientManager.parseTpd(this.resourceUri);
        if(fileList) {
            const filesInWorkspace = FileSystemManager.readWorkspace();
            if (!filesInWorkspace.size) {
                return;
            }
            let parents = new Set<string>();
            for(const file of fileList) {
                const parentsOfFile = FileSystemManager.getParentsOfUriInWorkspace(Uri.file(file));
                parents = new Set([...parents, ...parentsOfFile.map(item => item.toLowerCase())]);
            }
            const includedFiles = new Set([...fileList.map(file => file.toLowerCase()), ...parents, this.resourceUri.fsPath.toLowerCase()]);
            for(const file of includedFiles) {
                filesInWorkspace.delete(file);
            }
            return [...filesInWorkspace.values()];
        }
    }
}