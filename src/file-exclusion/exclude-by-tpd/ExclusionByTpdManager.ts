/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * 
 * @author Csilla Farkas
 */

import { window, TreeItem, Uri, workspace, TreeDataProvider, EventEmitter, Event, TreeItemCollapsibleState } from "vscode";
import { ConfigKey, ViewNames } from "../../common/types";
import { TpdFile } from "./TpdFile";
import { TpdFiles } from "./TpdFiles";
import { TpdFileDecorationProvider } from "./TpdFileDecorationProvider";
import { ExcludedFileDecorationProvider } from "../ExcludedFileDecorationProvider";
import { Modal } from "../../common/Modal";
import { ExclusionState } from "../ExclusionState";
import { clientManager } from "../../client/ClientManager";
import * as path from 'path';


export class ExclusionByTpdManager implements TreeDataProvider<TpdFile> {
    private _viewName = ViewNames.tpd;
    private _onDidChangeTreeData: EventEmitter<TpdFile | undefined | null | void> = new EventEmitter<TpdFile | undefined | null | void>();
    readonly onDidChangeTreeData?: Event<void | TpdFile | TpdFile[] | null | undefined> = this._onDidChangeTreeData.event;
    tpdFiles = new TpdFiles();

    constructor() {
        window.registerTreeDataProvider(this._viewName, this);
        window.createTreeView(this._viewName, { treeDataProvider: this});
    }

    public async init() {
        const activatedTpd = ExclusionState.activatedTpd;
        if(activatedTpd) {
            await this.activateTpd(activatedTpd);
        } else {
            const configProp = workspace.getConfiguration(ConfigKey.compiler);
            const isTpdRequired = configProp.get('analyzeOnlyWhenTpdIsActive') as boolean;
            if (!isTpdRequired) {
                await this.activateTpd();
                return;
            }
            const result = await Modal.allowFullProjectAnalyzisOrActivateTpd();
            if (result?.startsWith('Activate')) {
                await this.selectAndActivateTpd();
            } else if (result?.startsWith('Allow')) {
                await configProp.update('analyzeOnlyWhenTpdIsActive', false);
                await this.activateTpd();
            }
        }
    }

    public refresh() : void {
        this._onDidChangeTreeData.fire();
    }

    public decorateTpdFiles() {
        TpdFileDecorationProvider.decorate(this.tpdFiles.uris);
    }

    public getTreeItem(element: TpdFile): TreeItem {
        return element;
    }

    public getChildren(element?: TpdFile): TpdFile[] {
        return this.tpdFiles.items;
    }

    public async activateTpd(tpd: TpdFile | undefined = undefined): Promise<void> {
        if (!tpd) {
            await clientManager.parseTpd(null);
            return;
        }
        const prevActivatedTpd = this.tpdFiles.activatedTpd;
        const prevExcludedItems = ExclusionState.excludedItems.fsPaths;
        try {
            const success = await this._parseTpd(tpd);
            if(success) {
                const filesToBeDecorated = prevExcludedItems ? [...new Set([...prevExcludedItems, ...ExclusionState.excludedItems.fsPaths])].map(file => Uri.file(file)) : ExclusionState.excludedItems.items;
                await ExcludedFileDecorationProvider.decorate(filesToBeDecorated);
                this.decorateTpdFiles();
                return;
            }
            if (prevActivatedTpd) {
                this._resetPrevState(prevActivatedTpd);
            } else {
                const configProp = workspace.getConfiguration(ConfigKey.compiler);
                const isTpdRequired = configProp.get('analyzeOnlyWhenTpdIsActive') as boolean;
                if (!isTpdRequired) {
                    await clientManager.parseTpd(null);
                    return;
                }
                const result = await Modal.allowFullProjectAnalyzisOrActivateTpd();
                if (result?.startsWith('Activate')) {
                    await this.selectAndActivateTpd();
                } else if (result?.startsWith('Allow')) {
                    await configProp.update('analyzeOnlyWhenTpdIsActive', false);
                    await clientManager.parseTpd(null); 
                }
            }
        } catch (e) {
            console.log(e);
            this._resetPrevState(prevActivatedTpd);
            throw e;
        }
    }

    private _resetPrevState(prevActivatedTpd: TpdFile | void) {
        this.decorateTpdFiles();
        if(prevActivatedTpd) {
            this.tpdFiles.activateTpd(prevActivatedTpd);
        } else {
            this.tpdFiles.deactivateTpd();
        }
        this.refresh();
        this.decorateTpdFiles();
    }

    private async _parseTpd(tpd: TpdFile): Promise<boolean> {
        this.tpdFiles.activateTpd(tpd);
        this.refresh();
        const filesToBeExcluded = await tpd.getFilesToBeExcluded();
        if(filesToBeExcluded) {
            ExclusionState.excludedItems.add(filesToBeExcluded);
            await ExclusionState.saveActivatedTpd(tpd.resourceUri);
            return true;
        }
        return false;
    }

    public async deactivateTpd(): Promise<void> {
        await clientManager.deactivateTpd();
        const prevExcludedItems = ExclusionState.excludedItems.items;
        ExclusionState.excludedItems.remove();
        this.tpdFiles.deactivateTpd();
        await ExclusionState.resetActivatedTpd();
        this.refresh();
        this.decorateTpdFiles();
        await ExcludedFileDecorationProvider.decorate(prevExcludedItems);
    }

    public async selectAndActivateTpd() {
        const options = {
            filters: { tpd: ['tpd'] }
        };

        const selectedFiles = await window.showOpenDialog(options);
        if (selectedFiles) {
            for (const file of selectedFiles) {
                const tpd = new TpdFile(TpdFile.formatTpdName(file), TreeItemCollapsibleState.None, file);
                await this.tpdFiles.add(tpd);
                this.refresh();
                await this.activateTpd(tpd);
            }
        }
    }

    public async addNewTpd(open?: boolean) {
        const options = {
			filters: { tpd: [ 'tpd'] }
		};

		const selectedFiles = await window.showOpenDialog(options);
        if(selectedFiles) {
            for (const file of selectedFiles) {
                const tpd = new TpdFile(TpdFile.formatTpdName(file), TreeItemCollapsibleState.None, file);
                await this.tpdFiles.add(tpd);
                this.refresh();
                if(open) {
                    await this.openTpd(file);
                }
            }
        }
    }

    async openTpd(item: Uri): Promise<void> {
        const doc = await workspace.openTextDocument(item);
        await window.showTextDocument(doc);
    }

    public async removeTpd(tpd: TpdFile) {
        if(tpd.isActivated) {
            const fileName = path.basename(tpd.resourceUri.fsPath);
            const result = await Modal.removeActivatedTpd(fileName);
            if(!result) {
                return;
            }

            const configProp = workspace.getConfiguration(ConfigKey.compiler);
            if (configProp.get('analyzeOnlyWhenTpdIsActive') as boolean) {
                const result = await Modal.fullProjectAnalyzisIsForbidden("Deactivating TPD files");
                if (result === 'Turn off and analyze project') {
                    await configProp.update('analyzeOnlyWhenTpdIsActive', false);
                } else {
                    return;
                }
            }

            await ExclusionState.resetActivatedTpd();
            await ExcludedFileDecorationProvider.decorate(ExclusionState.excludedItems.items);
            ExclusionState.excludedItems.remove();
        }
        
        await this.tpdFiles.remove(tpd);
        this.refresh();
    }
}