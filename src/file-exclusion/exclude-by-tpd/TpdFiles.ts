/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * 
 * @author Csilla Farkas
 */

import { Uri } from "vscode";
import { TpdFile } from "./TpdFile";
import { TpdFilesConfig } from "../../common/Config";
import { ExclusionState } from "../ExclusionState";

export class TpdFiles {
    private _config = new TpdFilesConfig();
    private _items;
    constructor() {
        this._items = new Map<string, TpdFile>(this._config.content.map(tpdFile => [tpdFile.resourceUri.fsPath, tpdFile]));
        const savedActivatedTpd = ExclusionState.activatedTpd;
        if(savedActivatedTpd) {
            this._items.get(savedActivatedTpd.resourceUri.fsPath)?.activate();
        }
    }

    get items(): TpdFile[] {
        return [...this._items.values()];
    }

    get uris(): Uri[] {
        return this.items.map(tpd => tpd.resourceUri);
    }

    get activatedTpd(): TpdFile | void {
        return [...this._items.values()].find(tpd => tpd.isActivated);
    }

    public activateTpd(tpd: TpdFile) {
        const prevActivatedTpd = this.activatedTpd;
        prevActivatedTpd?.deactivate();
        const tpdFile = this._items.get(tpd.resourceUri.fsPath);
        tpdFile?.activate();
    }

    public deactivateTpd() {
        const activatedTpd = this.activatedTpd;
        if(activatedTpd) {
            activatedTpd.deactivate();
        }
    }

    public async add(tpd: TpdFile): Promise<void> {
        if(!this._items.has(tpd.resourceUri.fsPath)) {
            this._items.set(tpd.resourceUri.fsPath, tpd);
            await this._config.update([...this._items.keys()]);
        }
    }

    public async remove(tpd: TpdFile): Promise<void> {
        this._items.delete(tpd.resourceUri.fsPath);
        await this._config.update([...this._items.keys()]);
    }
}
