/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/

/**
 * @author Miklos Magyari
 */

import { Uri, workspace, window, ExtensionContext, ViewColumn, WebviewPanel } from "vscode";
import { catchError } from "./common/Errors";
import markdownit = require("markdown-it/lib");

export class DocumentationViewer {
     public readonly id = 'titan.showDocumentation';
	private context: ExtensionContext;
	private md = markdownit();
	private navbar: string = '';
	private style: string = '';
	private currentPage: string = '';
	private visitedPages: string[] = [];

	constructor(context: ExtensionContext) {
		this.context = context;

		const navbarpath = context.asAbsolutePath('documentation/navbar.md');
          const uri = Uri.file(navbarpath);
          workspace.openTextDocument(uri).then((doc) => {
			this.setNavbarText(doc.getText());
		});

		const stylepath = context.asAbsolutePath('documentation/style.css');
          const styleuri = Uri.file(stylepath);
          workspace.openTextDocument(styleuri).then((doc) => {
			this.setStyleText(doc.getText());
		});
	}
	
	@catchError
	public async showDocumentation() {
		this.currentPage = 'index.md';
		const path = this.context.asAbsolutePath('documentation/' + this.currentPage);
		const panel = window.createWebviewPanel(
			'docViewer',
			'TITAN documentation',
			ViewColumn.One,
			{
				enableScripts: true
			});
		
		panel.webview.onDidReceiveMessage(props => {
			if (props.a !== undefined) {
				const file = props.a.split('#')[0].split('?')[0].split('/').pop();
				this.visitedPages.push(this.currentPage);
				this.currentPage = file;
				const path = this.context.asAbsolutePath('documentation/' + file);
				this.renderFile(path, panel);
			}
			if (props.key === 'Backspace') {
				if (this.visitedPages.length > 0) {
					let backPage = this.visitedPages.pop() ?? '';
					this.currentPage = backPage;
					const path = this.context.asAbsolutePath('documentation/' + backPage);
					this.renderFile(path, panel);
				}
			}
		});
		
		this.renderFile(path, panel);
	}

	renderFile(path: string, panel: WebviewPanel) {
		workspace.openTextDocument(Uri.file(path)).then((doc) => {
			this.renderMarkdownToWebview(doc.getText(), panel);
		});
	}

	renderMarkdownToWebview(text: string, panel: WebviewPanel) {
		const localPath = Uri.joinPath(this.context.extensionUri, 'documentation');
		const imgsrc = panel.webview.asWebviewUri(localPath);
		const re = /(img src=\")([\/\w\-_.]+)\"/gm;
		const page = `
		<style>

		${this.style}

		</style>

		<div class='navbar'>

		${this.md.render(this.navbar)}

		</div>
		<div class='main'>

		${this.md.render(text)}

		</div>
		`;

		let result = page + this.getScript();
		panel.webview.html = result.replace(re, '$1' + `${imgsrc}/` + '$2\"');
	}

	setNavbarText(navbartext: string) {
		this.navbar = navbartext;
	}

	setStyleText(styletext: string) {
		this.style = styletext;
	}
	
	getScript() {
	return `	
	<script>
	
	const vscode = acquireVsCodeApi();
	document.addEventListener('click', e => {
		let node = e && event.target;
		while(node) {
			if (node.tagName && node.tagName === 'A' && node.href) {
				console.log(node);
				vscode.postMessage({
					a: node.href,
				});
			}
			node = node.parentNode;
		}
	}, true);
	document.addEventListener('keyup', e => {
		vscode.postMessage({
			key: e.key
		});
	}, true);
	
	</script>
	`;
	}
}
