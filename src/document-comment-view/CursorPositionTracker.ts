/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Csilla Farkas
 */

import { window, Position } from "vscode";

export class CursorPositionTracker {
	private _lastPosition: Position | undefined;

	get current(): Position | undefined {
		return window.activeTextEditor?.selection.active;
	}

	set lastPosition(position: Position | undefined ) {
		this._lastPosition = position;
	}

	get last(): Position | void {
		return this._lastPosition;
	}

	isCursorPositionChanged(): boolean {
		return this.last && this.current ? !this.current.isEqual(this.last) : true;
	}
}