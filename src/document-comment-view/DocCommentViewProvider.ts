/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Csilla Farkas
 */

import { WebviewViewProvider, WebviewView, WebviewViewResolveContext, CancellationToken, TextEditor, Selection, Position, TextEditorSelectionChangeEvent } from "vscode";
import { settings } from "../common/Settings";
import { Timer } from "../common/Timer";
import { clientManager } from "../client/ClientManager";
import * as md from 'markdown-it';
import { CursorPositionTracker } from "./CursorPositionTracker";

export class DocCommentViewProvider implements WebviewViewProvider {

	public static readonly viewType = 'docCommentView';

	private _view?: WebviewView;
	private _defaultHtml: string = "";
	
	public timer = new Timer();
	public cursorPosition = new CursorPositionTracker();

	public resolveWebviewView(
		webviewView: WebviewView,
		context: WebviewViewResolveContext,
		_token: CancellationToken,
	) {
		this._view = webviewView;
        webviewView.webview.html = this._defaultHtml;
	}

	public set view(html: string) {
        if(this._view) {
            // This should be a complete, valid html document. Changing this property causes the webview to be reloaded.
            this._view.webview.html = html;
        }
    }

	public mdToHtml(markdownText: string): string {
		const markdown = new md({html: true});
        const html = markdown.render(markdownText);
		return html;
	}

	public createDocCommentRequestParams(doc: TextEditor, selections: readonly Selection[]): { identifier: { uri: string }, position: Position } {
		const cursorPosition = selections[0].active;
		const requestParams = {
			identifier: { uri: doc.document.uri.toString() },
			position: cursorPosition
		};

	   	return requestParams;
	}

	public sendRequestForDocCommentIfCursorPositionChanged(event: TextEditorSelectionChangeEvent) {
		this.timer.clearTimer();
		this.cursorPosition.lastPosition = this.cursorPosition.current;
       	this.timer.startTimer(this.getDocComment, event);
	}

	public getDocComment = async (event: TextEditorSelectionChangeEvent) => {
		const { textEditor, selections, kind } = event;
		const requestParams = this.createDocCommentRequestParams(textEditor, selections);
		if(!this.cursorPosition.isCursorPositionChanged()) {
			if(selections.length > 1 || !settings.isSupportedFileType(textEditor.document.languageId)) {return;}
			const docComment = await clientManager.getDocumentComment(requestParams);
			if(docComment) {
				this.view = this.mdToHtml(docComment);
			}
		}
	};
}


export const docCommentViewProvider = new DocCommentViewProvider();