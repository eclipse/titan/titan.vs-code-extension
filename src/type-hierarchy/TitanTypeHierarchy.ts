/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
* @author Csilla Farkas
*/

import { CancellationToken, Position, TextDocument, TypeHierarchyItem, TypeHierarchyProvider, Uri} from "vscode";
import { clientManager } from "../client/ClientManager";

export class TitanTypeHierarchyProvider implements TypeHierarchyProvider {
    async prepareTypeHierarchy(document: TextDocument, position: Position, token: CancellationToken) {
        const requestParams = {
            textDocument: { uri: document.uri.toString() },
            position: position
        };
        const data = await clientManager.prepareTypeHierarchy(requestParams) as TypeHierarchyItem[];
        if(data) {
            const typeHierarchyItems = data.map(item => {
                return {
                    ...item,
                    uri: Uri.parse(item.uri.toString())
                };
            });

            return typeHierarchyItems;
        }
        return null;
    };

    async provideTypeHierarchySubtypes(item: TypeHierarchyItem, token: CancellationToken) {
        const data = await clientManager.resolveSupertypes(item) as TypeHierarchyItem[];
        if(data) {
            const typeHierarchyItems = data.map(item => {
                return {
                    ...item,
                    uri: Uri.parse(item.uri.toString())
                };
            });

            return typeHierarchyItems;
        }
        return null;
    };

    async provideTypeHierarchySupertypes(item: TypeHierarchyItem, token: CancellationToken) {
        const data = await clientManager.resolveSubtypes(item) as TypeHierarchyItem[];
        if(data) {
            const typeHierarchyItems = data.map(item => {
                return {
                    ...item,
                    uri: Uri.parse(item.uri.toString())
                };
            });

            return typeHierarchyItems;
        }
        return null;
    }
}

export const titanTypeHierarchyProvider = new TitanTypeHierarchyProvider();