# Titan - Visual Studio Code and Eclipse Theia extension

Thanks for your interest in this project.

## Project description
This extension provides TTCN-3 and ASN.1 language support for Visual Studio Code and Eclipse Theia powered by [Titan language server](https://gitlab.eclipse.org/eclipse/titan/titan.language-server) using [LSP4j](https://projects.eclipse.org/projects/technology.lsp4j).
Both projects are in experimental phase.

[More about Titan](https://projects.eclipse.org/projects/tools.titan)

## Features

Titan language server performs full syntactic and semantic analysis of a TTCN-3 project.\
The current status of the supported and planned features:

✅ Code completion\
✅ Diagnostics\
✅ Document symbols\
✅ Go to definition\
✅ Go to type definition\
✅ Hover (displaying combined information based on semantic analysis and TTCN-3 document comments)\
✅ Peek definition\
✅ Signature help\
✅ Snippets\
✅ Code action (proposals for code fixes)\
✅ Find references\
✅ Semantic highlight\
✅ Code lens\
❌ Document highlight\
❌ Folding range\
❌ Formatting\
✅ Rename\
Additional features:\
✅ Excluding/including files in a project\
✅ Building up projects based on TPD files\
❌ Makefile generation

## Supported file types

The following file types are handled by the extension:
- TTCN-3: `.ttcn, .ttcn3, .ttcnpp, .ttcnin`
- ASN.1: `.asn, .asn1`
- Runtime configuration files: `.cfg`
- Titan project descriptor files: `.tpd`

## Minimum requirements
This extension has the following minimum requirements:
* Visual Studio Code 1.71.0
* [Titan Language Server minimum requirements](https://gitlab.eclipse.org/eclipse/titan/titan.language-server#minimum-requirements)

## Contact
If you found a bug or have a feature request, contact the project developers:
https://projects.eclipse.org/projects/tools.titan/contact

## Building the code from source
Follow this high level instructions to build the extension and the language server from source:
1. Install [NodeJS](https://nodejs.org/) and [Maven](https://maven.apache.org/index.html)
    - Via [package manager](https://nodejs.org/en/download/package-manager/)\
    `apt-get install -y nodejs mvn`
2. Install the [vsce](https://code.visualstudio.com/api/working-with-extensions/publishing-extension#vsce) tool\
    `npm install -g @vscode/vsce`
3. Clone [this](https://gitlab.eclipse.org/eclipse/titan/titan.vs-code-extension) and the [language server](https://gitlab.eclipse.org/eclipse/titan/titan.language-server) repositories\
    `git clone https://gitlab.eclipse.org/eclipse/titan/titan.vs-code-extension.git`\
    `git clone https://gitlab.eclipse.org/eclipse/titan/titan.language-server.git`
4. Build and package the language server using Maven\
    `cd titan.language-server/lsp`\
    `mvn package`
5. Install dependencies for the extension\
    `cd titan.vs-code-extension`\
    `npm install`
5. Copy the compiled Java archive and the language server dependencies to the extension's folder\
    `mkdir titan.language-server/server`\
    `cp -r titan.language-server/lsp/target/lib titan.vs-code-extension/server`\
    `cp titan.language-server/lsp/target/org.eclipse.titan.lsp*.jar titan.vs-code-extension/server/org.eclipse.titan.lsp.jar`
6. [Package](https://code.visualstudio.com/api/working-with-extensions/publishing-extension#usage) the extension using vsce\
    `vsce package`

At the end you will have `titan-x.y.z.vsix` file that you can install to VS Code and Eclipse Theia.
